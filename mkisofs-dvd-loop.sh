#!/bin/sh
DEST=$PWD
SRC="$( cd "$(dirname "$0")" ; pwd -P )"
echo Converting from ${SRC} into ${DEST}
cd ${SRC}
find -maxdepth 1 -type d -not -name '.*' -print -exec mkisofs --dvd-video -o $DEST/'{}'.dvd.iso $SRC/'{}' \;
cd ${DEST}
