#!/bin/sh
DEST=$PWD
#SRC="$( cd "$(dirname "$0")" ; pwd -P )"
SRC=/var/cache/moviez/vob
echo Converting from ${SRC} into ${DEST}
cd ${SRC}
find -maxdepth 1 -type d -not -name '.*' -print -exec mkisofs -o $DEST/'{}'.iso $SRC/'{}' \;
cd ${DEST}
