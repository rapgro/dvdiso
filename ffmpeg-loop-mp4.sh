#!/bin/sh
DEST=$PWD
SRC="$( cd "$(dirname "$0")" ; pwd -P )"
CACHE=~/moviez/cache/vob
echo Converting from ${SRC} into ${DEST}
cd $SRC
OIFS="$IFS"
IFS=$'\n'
for name in $(find -maxdepth 1 -type d -not -name '.*' -print) ; do
 cd "$name"/VIDEO_TS
 echo generate $name.vob ...
 find -name "VTS_*_0.*" -delete ; cat $(ls VTS_*.VOB) >"$CACHE/$name.vob"
 echo done $name.vob 
 cd ../..
 ffmpeg -y -analyzeduration 100M -probesize 100M -i "$CACHE/$name.vob" -map 0 -map -0:s -map -0:0 -c copy "$DEST/$name.mp4"
 echo done $name.mp4
done
cd ${DEST}
