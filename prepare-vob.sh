#!/bin/sh
find -type f -not -name '*.VOB' -print -delete
find -type d -maxdepth 1 -print -exec mv '{}' '{}'.vob
